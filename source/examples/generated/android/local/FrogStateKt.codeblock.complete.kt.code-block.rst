.. code-block:: kotlin

   enum class FrogStateKt(val state: String) {
       TADPOLE("Tadpole"),
       FROG("Frog"),
       OLD_FROG("Old Frog");
   }
