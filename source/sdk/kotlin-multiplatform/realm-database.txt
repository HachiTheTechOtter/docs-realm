.. _kotlin-multiplatform-realm-database:

=========================================
Realm Database - Kotlin Multiplatform SDK
=========================================

.. default-domain:: mongodb

.. contents:: On this page
   :local:
   :backlinks: none
   :depth: 2
   :class: singlecol

.. include:: /includes/realm-database.rst
