.. _web-manage-email-password-users:

===========================
Manage Email/Password Users
===========================

.. default-domain:: mongodb

.. contents:: On this page
   :local:
   :backlinks: none
   :depth: 2
   :class: singlecol

Overview
--------

The Web SDK includes a client object that allows you to manage users associated
with the :doc:`Email/Password authentication provider
</authentication/email-password>`.

.. _web-email-password-register-new-user: 

Register a New User Account
---------------------------

To register a new email/password user, call the ``registerUser()`` method with
the user's email address and desired password. The email address must not be
associated with another email/password user and the password must be between 6
and 128 characters.

.. code-block:: javascript

   const email = "someone@example.com";
   const password = "Pa55w0rd";
   await app.emailPasswordAuth.registerUser({ email, password });

.. note:: Confirm New Users

   You must :ref:`confirm a new user's email address
   <web-email-password-confirm-user>` before they can log in to your app.

.. _web-email-password-confirm-user:

Confirm a New User's Email Address
----------------------------------

New users must confirm that they own their email address before they can log in
to your app unless the provider is configured to :ref:`automatically confirm new
users <auth-automatically-confirm-users>`.

If the provider is configured to :ref:`send a confirmation email
<auth-send-a-confirmation-email>`, {+service+} automatically sends a
confirmation email when a user registers. The email contains a link to the
configured :guilabel:`Email Confirmation URL` with a token that is valid for 30
minutes after the email is sent. If a user did not receive the initial email or
didn't click the confirmation link in time, you can use the SDK to :ref:`resend a
confirmation email <web-email-password-resend-confirmation-email>`.

Alternatively, if the provider is configured to :ref:`run a confirmation
function <auth-run-a-confirmation-function>`, {+service+} automatically runs
your custom :doc:`{+service-short+} Function </functions>` when a user registers. 
If the call to the custom confirmation function fails, you can use the SDK to :ref:`resend a
confirmation email <web-email-password-resend-confirmation-function>`.

.. _web-email-password-complete-confirmation:

Complete a User Confirmation
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

You need a valid ``token`` and ``tokenId`` for a registered user in order to
confirm them and allow them to log in. These values are available in different
places depending on the provider configuration:

- If the provider is set to :ref:`send a confirmation email
  <auth-send-a-confirmation-email>`, the ``token`` and ``tokenId`` values are
  included as query parameters in the :guilabel:`Email Confirmation URL`.

- If the provider is set to :ref:`run a confirmation function
  <auth-run-a-confirmation-function>`, the ``token`` and ``tokenId`` values are
  passed to the function as arguments.

To confirm a registered user, call the ``confirmUser()`` method with the user's
valid ``token`` and ``tokenId``:

.. code-block:: javascript

   await app.emailPasswordAuth.confirmUser({ token, tokenId });

.. _web-email-password-retry-user-confirmation:

Retry User Confirmation Methods
-------------------------------

The SDK provides methods to resend user confirmation emails or retry custom 
confirmation methods.

.. _web-email-password-resend-confirmation-email:

Resend a Confirmation Email
~~~~~~~~~~~~~~~~~~~~~~~~~~~

To resend the confirmation email to a user, call the ``resendConfirmationEmail()``
method with the user's email address:

.. code-block:: javascript
   
   const email = "someone@example.com"; // The user's email address
   await app.emailPasswordAuth.resendConfirmationEmail({ email });

.. _web-email-password-resend-confirmation-function:

Retry a User Confirmation Function
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. versionadded:: Realm Web v1.4.0

To re-run your :ref:`custom confirmation function
<auth-run-a-confirmation-function>`, call the ``retryCustomConfirmation()`` method
with the user's email address:

.. code-block:: javascript

   const email = "someone@example.com"; // The user's email address
   await app.emailPasswordAuth.retryCustomConfirmation({ email });

.. _web-email-password-reset-password:

Reset a User's Password
-----------------------

.. _web-send-password-reset-email:

Send a Password Reset Email
~~~~~~~~~~~~~~~~~~~~~~~~~~~

If the provider is configured to :ref:`send a password reset email
<auth-send-a-password-reset-email>`, you can use the SDK to send a password
reset email to a user. The email contains a link to the configured
:guilabel:`Password Reset URL`.

.. code-block:: javascript

   // The user's email address
   const email = "joe.jasper@example.com"
   await app.emailPasswordAuth.sendResetPasswordEmail({ email });

.. _web-call-password-reset-function:

Call a Password Reset Function
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

If the provider is configured to :ref:`run a password reset function
<auth-run-a-password-reset-function>`, you can use the SDK to run the function.

.. code-block:: javascript

   // The user's email address
   const email = "joe.jasper@example.com";
   // The new password to use
   const password = "newPassw0rd";
   // Additional arguments for the reset function
   const args = [];

   await app.emailPasswordAuth.callResetPasswordFunction({ email, password }, args);

.. _web-email-password-complete-password-reset:

Complete a Password Reset
~~~~~~~~~~~~~~~~~~~~~~~~~

Once a user requests a password reset, either by :ref:`sending a password reset
email <web-send-password-reset-email>` or :ref:`calling a password reset
function <web-call-password-reset-function>`, Realm generates a pair of unique
``token`` and ``tokenId`` values that they can use to complete the password
reset within 30 minutes of the initial request.

.. code-block:: javascript

   await app.emailPasswordAuth.resetPassword({ password: "newPassw0rd", token, tokenId });

.. example:: Get the Token and TokenID

   If the provider uses the built-in password reset email, the ``token`` and
   ``tokenId`` are included as query parameters in the password reset URL. You
   can access them like so:

.. code-block:: javascript

   const params = new URLSearchParams(window.location.search);
   const token = params.get("token");
   const tokenId = params.get("tokenId");
   if (!token || !tokenId) {
      throw new Error(
         "You can only call resetPassword() if the user followed a confirmation email link"
      );
   }
