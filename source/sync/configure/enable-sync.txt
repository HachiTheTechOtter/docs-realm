.. _enable-realm-sync:
.. _enable-sync:

=================
Enable Realm Sync
=================

.. default-domain:: mongodb

.. contents:: On this page
   :local:
   :backlinks: none
   :depth: 2
   :class: singlecol

Overview
--------

You can enable {+sync+} via the {+app+} UI, Realm CLI, or the 
Realm Admin API. If it is your first time enabling {+sync-short+}, 
the UI is a great choice because it walks you through the required steps.

If you're re-enabling it after pausing or terminating it, see: 
:ref:`Resume <resume-sync>` or :ref:`Re-Enable <re-enable-realm-sync>` 
{+sync-short+}.

Prerequisites
-------------

While you are configuring {+sync+}, you must define the data access patterns
and rules for your {+app+}. If you haven't already decided how you want to 
configure your data model and access {+sync-short+}, see:

- :ref:`Configure Your Data Model <sync-schema-overview>`
- :ref:`Partitions <sync-partitions>`
- :ref:`Sync Rules and Permissions <sync-permissions>`

Procedure
---------

.. important::
   
   You must define at least one valid :ref:`schema <schemas>` for a collection 
   in the synced cluster before you can define sync rules and enable sync.
   
   At minimum, the schema must define ``_id`` and the field that you intend to
   use as your :term:`partition key`. A partition key field may be a ``string``,
   ``integer``, or ``objectId``.
   
   For more details on how to define a schema, see :ref:`enforce-a-schema`.

.. tabs-realm-admin-interfaces::

   .. tab::
      :tabid: ui
      
      .. include:: /includes/steps/define-sync-rules-ui.rst
     
   .. tab::
      :tabid: cli
      
      .. include:: /includes/steps/define-sync-rules-cli.rst
     
   .. tab::
      :tabid: api
      
      .. include:: /includes/steps/define-sync-rules-api.rst
